﻿using System;
using System.Windows.Forms;

namespace TradingGame2018
{
    public partial class Form1 : Form
    {
        // Initialise the arrays we will need later
        Label[] items = new Label[4];
        Label[] prices = new Label[4];
        Label[] quant = new Label[4];
        
        // Seed the random number
        Random rand = new Random();

        // Create a structure that will allow us to group
        // information on each location to a single index
        public struct Locations
        {
            // Add the name of each location to the structure
            public string itemName;

            // In this version, price is 1 to 
            // whatever value you put in price
            public int price1;

            // variation is whatever you multiply
            // the base price by to make it lean high,
            // low or neutral
            public double variation1;

            public int price2;
            public double variation2;
            public int price3;
            public double variation3;
            public int price4;
            public double variation4;
        }

        // Create an array of locations
        Locations[] shops = new Locations[4];

        public Form1()
        {
            InitializeComponent();

            // Make sure all buy button clicks 
            // go to the same place
            buy2.Click += buy1_Click;
            buy3.Click += buy1_Click;
            buy4.Click += buy1_Click;

            //sell1.Click += sell_Click;
            sell2.Click += sell_Click;
            sell3.Click += sell_Click;
            sell4.Click += sell_Click;

        }

        private void sell_Click(object sender, EventArgs e)
        {
            Button sentButton = (Button)sender;

            int index = int.Parse(sentButton.Tag.ToString());

            // Check that there are items to sell
            if (int.Parse(quant[index].Text) >= 1)
            {
                money.Text = (double.Parse(money.Text) + double.Parse(prices[index].Text)).ToString();
                quant[index].Text = (int.Parse(quant[index].Text) - 1).ToString();
            }
        }

        private void buy1_Click(object sender, EventArgs e)
        {
            // Cast the sender object as a button
            Button sentButton = (Button)sender;
            int index = int.Parse(sentButton.Tag.ToString());

            // Check that money is available to buy
            if (double.Parse(money.Text) - double.Parse(prices[index].Text)>=0) {
                money.Text = (double.Parse(money.Text) - double.Parse(prices[index].Text)).ToString();
                quant[index].Text = (int.Parse(quant[index].Text) + 1).ToString();
            }
        }
        
        private void setUpGame()
        {
            // Set up the arrays we will need
            items[0] = item1;
            items[1] = item2;
            items[2] = item3;
            items[3] = item4;

            prices[0] = price1;
            prices[1] = price2;
            prices[2] = price3;
            prices[3] = price4;

            quant[0] = quant1;
            quant[1] = quant2;
            quant[2] = quant3;
            quant[3] = quant4;

            // Populate the structures
            // Note: there are better ways of doing this
            shops[0].itemName = "Apples";
            shops[0].price1 = 10;
            shops[0].variation1 = 1.2;
            shops[0].price2 = 20;
            shops[0].variation2 = 1.4;
            shops[0].price3 = 30;
            shops[0].variation3 = 1.2;
            shops[0].price4 = 40;
            shops[0].variation4 = 0.8;

            shops[1].itemName = "Pears";
            shops[1].price1 = 12;
            shops[1].variation1 = 0.9;
            shops[1].price2 = 10;
            shops[1].variation2 = 1.8;
            shops[1].price3 = 15;
            shops[1].variation3 = 1.9;
            shops[1].price4 = 30;
            shops[1].variation4 = 0.8;
            
            shops[2].itemName = "Oranges";
            shops[2].price1 = 11;
            shops[2].variation1 = 1.9;
            shops[2].price2 = 14;
            shops[2].variation2 = 1.1;
            shops[2].price3 = 25;
            shops[2].variation3 = 1.2;
            shops[2].price4 = 10;
            shops[2].variation4 = 2.8;
            
            shops[3].itemName = "Plums";
            shops[3].price1 = 8;
            shops[3].variation1 = 1.9;
            shops[3].price2 = 1;
            shops[3].variation2 = 1.8;
            shops[3].price3 = 5;
            shops[3].variation3 = 1.9;
            shops[3].price4 = 3;
            shops[3].variation4 = 3.8;

            // Load the item names
            for(int i = 0; i < items.Length; i++)
            {
                items[i].Text = shops[i].itemName;
            }

            // load the first location
            locations.SelectedIndex = 0;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Initialise the screen and components
            setUpGame();
        }
        
        private void locations_SelectedIndexChanged(object sender, EventArgs e)
        {
            // This event is triggered every time the 
            // combobox is changed

            // Cast the sender object as a combobox
            ComboBox sentCombo = (ComboBox)sender;

            // Set the prices
            // Formula here is random number between
            // 1 and the price set in the structure
            // multiplied by the variation
            price1.Text = (rand.Next(1, shops[sentCombo.SelectedIndex].price1) * shops[sentCombo.SelectedIndex].variation1).ToString();
            price2.Text = (rand.Next(1, shops[sentCombo.SelectedIndex].price2) * shops[sentCombo.SelectedIndex].variation2).ToString();
            price3.Text = (rand.Next(1, shops[sentCombo.SelectedIndex].price3) * shops[sentCombo.SelectedIndex].variation3).ToString();
            price4.Text = (rand.Next(1, shops[sentCombo.SelectedIndex].price4) * shops[sentCombo.SelectedIndex].variation4).ToString();
            
        }
    }
}
